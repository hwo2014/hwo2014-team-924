#ifndef LOG_H_
#define LOG_H_

//#include <iostream>
//#include <string>
#include <sstream>
//#include <fstream>

//#define LOG_LEVEL 0   //no logs
//#define LOG_LEVEL   1   //Log1
//#define LOG_LEVEL 2   //Log1, Log2

#if (LOG_LEVEL == 0)

#define Log1(arg) do{ } while(0)
#define Log2(arg) do{ } while(0)

#elif (LOG_LEVEL == 1)

#define Log1(arg) do{ std::ostringstream os; os<<arg; CPPLog(os.str());} while(0)
#define Log2(arg) do{ } while(0)

#elif (LOG_LEVEL == 2)

#define Log1(arg) do{ std::ostringstream os; os<<arg; CPPLog(os.str());} while(0)
#define Log2(arg) do{ std::ostringstream os; os<<arg; CPPLog(os.str());} while(0)

#endif//LOG_LEVEL


void CPPLog(const std::string &os);

#endif /* LOG_H_ */
