#ifndef PIECE_H_
#define PIECE_H_

#include <list>
#include <vector>
#include <sstream>
#include <memory>

class Piece {
public:
    Piece() {}
    virtual ~Piece() {}

    virtual std::string ToString() = 0;
};
typedef std::vector<Piece*> Pieces;

class Straight: public Piece{
public:
    Straight(int aLength, bool aSwitch=false, bool aBridge=false)
        :Length(aLength), Switch(aSwitch), Bridge(aBridge) {}
    virtual ~Straight() {}

    int Length;
    bool Switch;
    bool Bridge;

    //  friend std::ostream& operator
    virtual std::string ToString()
    {
        std::ostringstream str;
        str << "Length: "<< Length
            << "|\t#Switch: " << Switch
            << "|\t#Bridge: " << Bridge;
        return str.str();
    }
};

class Bend: public Piece {
public:
    Bend(double aAngle, int aRadius) : Angle(aAngle), Radius(aRadius) {}
    virtual ~Bend(){}

    double Angle;
    int Radius;

//  friend std::ostream& operator
    virtual std::string ToString()
    {
        std::ostringstream str;
        str << "Angle: "<< Angle
            << "|\t#Radius: " << Radius;
        return str.str();
    }
};

class Lane {
public:
    Lane(int aId, int aDistance) : Distance(aDistance), Id(aId) {}
    virtual ~Lane() {}

    int Distance;
    int Id;

//  friend std::ostream& operator
    virtual std::string ToString()
    {
        std::ostringstream str;
        str << "Id: "<< Id
            << "|\t#Distance: " << Distance;
        return str.str();
    }
};
typedef std::list<Lane*> Lanes;

class Car {
public:
    Car() {}
    virtual ~Car() {}
    std::string Name;
    std::string Color;


//  friend std::ostream& operator
    virtual std::string ToString()
    {
        std::ostringstream str;
        str << "Name: "<< Name
            << "|\t#Color: " << Color;
        return str.str();
    }
};
typedef std::unique_ptr<Car> CarType; //TODO do others

class Position {
public:
    Position():
        Angle(0),
        PieceId(0),
        InPieceDistance(0),
        LaneStartLaneId(0),
        LaneEndLaneId(0) {}
    virtual ~Position() {}
    double Angle;
    int PieceId;
    double InPieceDistance;
    int LaneStartLaneId;
    int LaneEndLaneId;

//  friend std::ostream& operator
    virtual std::string ToString()
    {
        std::ostringstream str;
        str << "Angle: "<< Angle
            << "|\tPieceId: " << PieceId
            << "|\tInPieceDistance: " << InPieceDistance
            << "|\tLaneStartLaneId: " << LaneStartLaneId
            << "|\tLaneEndLaneId: " << LaneEndLaneId;
        return str.str();
    }
};




#endif /* PIECE_H_ */
