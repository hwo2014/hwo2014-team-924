#ifndef TRACKREADER_H_
#define TRACKREADER_H_

#include <iostream> //TODO std::cout
#include <string>
#include "Log.h"
#include "system_header.h"
#include "Structs.h"
#include "Track.h"

//TODO autoptr

class TrackReader {
public:
    virtual ~TrackReader() {Log2("DTOR TrackReader");}
    static TrackReader &getInstance();


    TrackPtr getTrack(const jsoncons::json& data);

private:
    TrackReader(): m_track(0) {Log2("CTOR TrackReader");}

    void resetData(const jsoncons::json& data);
    std::string getName();
    std::string getId();
    Pieces getPieces();
    Lanes getLanes();
    //TODO startingPoint

    const jsoncons::json* m_track;
};

#endif /* TRACKREADER_H_ */
