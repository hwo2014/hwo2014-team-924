/*
 * PositionReader.cpp
 *
 *  Created on: 20 kwi 2014
 *      Author: Gosia
 */

#include <string>
#include "PositionReader.h"
#include "State.h"
#include "Structs.h"


PositionReader& PositionReader::getInstance()
{
    static PositionReader instance;
    return instance;
}

Position PositionReader::getPosition(const jsoncons::json& data)
{
    resetData(data);

    Position pos;

    for(int i=0; i<m_position->size();++i)
    {
        std::string name = (*m_position)[i]["id"]["name"].as<std::string>(); //TODO not needed
        std::string color = (*m_position)[i]["id"]["color"].as<std::string>();

        if(color != State::getInstance().car->Color)
            continue;


        pos.Angle = (*m_position)[i]["angle"].as<double>();
        pos.PieceId = (*m_position)[i]["piecePosition"]["pieceIndex"].as<int>();
        pos.InPieceDistance = (*m_position)[i]["piecePosition"]["inPieceDistance"].as<double>();
        pos.LaneStartLaneId = (*m_position)[i]["piecePosition"]["lane"]["startLaneIndex"].as<int>();
        pos.LaneEndLaneId = (*m_position)[i]["piecePosition"]["lane"]["endLaneIndex"].as<int>();
//        int lap = (*m_position)[i]["piecePosition"]["lap"].as<int>();

        Log1(pos.ToString());
        break;
    }

    return pos;
}

void PositionReader::resetData(const jsoncons::json& data)
{
    m_position = &(data["data"]);
}
