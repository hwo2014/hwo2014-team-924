/*
 * State.h
 *
 *  Created on: 20 kwi 2014
 *      Author: Gosia
 */

#ifndef STATE_H_
#define STATE_H_

#include <iostream>
#include "Log.h"
#include "Structs.h"
#include "Track.h"

class State {
public:
    static State &getInstance();
    virtual ~State() {Log2("DTOR State");}//TODO Logger needed (off in release??)

    CarType car;
    TrackPtr track;
    double speed;
private:
    State() :speed(1.0) {Log2("CTOR State");}
};

#endif /* STATE_H_ */
