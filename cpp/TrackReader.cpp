/*
 * TrackReader.cpp
 *
 *  Created on: 19 kwi 2014
 *      Author: Gosia
 */

#include "TrackReader.h"

void TrackReader::resetData(const jsoncons::json& data)
{
    m_track = &(data["data"]["race"]["track"]);
}

TrackReader& TrackReader::getInstance()
{
    static TrackReader instance;
    return instance;
}

std::string TrackReader::getName()
{
    return (*m_track)["name"].as<std::string>();
}

std::string TrackReader::getId()
{
    return (*m_track)["id"].as<std::string>();
}

Pieces TrackReader::getPieces()
{
    Pieces ret;
    const auto& data = (*m_track)["pieces"];

    for(int i=0; i<data.size();++i)
    {
        if(data[i].has_member("length"))
        {
            int Length = data[i]["length"].as<int>();
            bool Switch = false;
            if(data[i].has_member("switch"))
                if(data[i]["switch"].as<std::string>() == "true")
                    Switch = true;

            bool Bridge = false;
            if(data[i].has_member("bridge"))
                if(data[i]["bridge"].as<std::string>() == "true")
                    Bridge = true;

            Straight *str = new Straight(Length,Switch,Bridge);
            ret.push_back(str);
        }
        else if(data[i].has_member("radius"))
        {
            int Radius = data[i]["radius"].as<int>();
            double Angle = data[i]["angle"].as<double>();

            Bend *bend = new Bend(Angle,Radius);
            ret.push_back(bend);
        }
    }

    // no copy according to c++11
    return ret;
}

Lanes TrackReader::getLanes()
{
    Lanes ret;
    const auto& data = (*m_track)["lanes"];

    for(int i=0; i<data.size();++i)
    {
        int dist = data[i]["distanceFromCenter"].as<int>();
        int id = data[i]["index"].as<int>();
        Lane *lane = new Lane(id,dist);
        ret.push_back(lane);
    }

    // no copy according to c++11
    return ret;
}

TrackPtr TrackReader::getTrack(const jsoncons::json& data)
{
    resetData(data);

    TrackPtr track(new Track());

    track->pieces = getPieces();
    track->lanes = getLanes();
    track->Id = getId();
    track->Name = getName();

    return track;
}
