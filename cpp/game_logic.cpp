#include "game_logic.h"
#include "protocol.h"
#include "TrackReader.h"
#include "CarReader.h"
#include "PositionReader.h"
#include "State.h"
#include "Log.h"

using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
      { "gameInit", &game_logic::on_game_init },
      { "join", &game_logic::on_join },
      { "yourCar", &game_logic::on_your_car },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "lapFinished", &game_logic::on_tick }
    }
{
}

game_logic::~game_logic()
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
//  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, msg);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  Log1("on join");
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
  Log1("On yourCar");

  State::getInstance().car = CarReader::getInstance().getCar(data);
  Log1(State::getInstance().car->ToString());


  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  Log1("On gameStart");
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
  Log2("On carPositions");

  Position position = PositionReader::getInstance().getPosition(data);

  Piece * current= State::getInstance().track->pieces[position.PieceId];
  Piece * next = 0;
  if(State::getInstance().track->pieces.size()>=position.PieceId+1)
	  next= State::getInstance().track->pieces[position.PieceId+1];
  Straight* curr_str = dynamic_cast<Straight*>(current);
  Bend* curr_bend = dynamic_cast<Bend*>(current);

  Straight* next_str = 0;
  Bend* next_bend = 0;
  if(next){
	  next_str = dynamic_cast<Straight*>(next);
	  next_bend = dynamic_cast<Bend*>(next);
  }

  if(curr_str && next_str)
  {
	  State::getInstance().speed = 1.0;
  }
  else if(curr_str && next_bend)
  {
	  State::getInstance().speed = 0.5;
  }
  else if(curr_bend && next_bend)
  {
      State::getInstance().speed = 0.5;
  }
  else if(curr_bend && next_str)
  {
      State::getInstance().speed = 1.0;
  }
  else if(!next_str && !next_bend && curr_str)
	  State::getInstance().speed = 1.0;
  else if(!next_str && !next_bend && curr_bend)
  	  State::getInstance().speed = 0.5;
  else
  	  State::getInstance().speed = 0.5;//TODO not needed?

//  return { ThrottleAnalyzer::getThrottle(data) };

  return { make_throttle(State::getInstance().speed) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  Log1("On crash");
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
    Log1("On gameEnd");
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
    Log1("On gameInit");

  //building track data
  State::getInstance().track = TrackReader::getInstance().getTrack(data);
  Log1(State::getInstance().track->ToString());

  return { make_ping() };
}

game_logic::msg_vector game_logic::on_tick(const jsoncons::json& data)
{
  std::cout << "tick received\n";//TODO
  std::cout << data.to_string() << std::endl;
  return { make_ping() };
}
