/*
 * CarReader.h
 *
 *  Created on: 20 kwi 2014
 *      Author: Gosia
 */

#ifndef CARREADER_H_
#define CARREADER_H_

#include "Log.h"
#include "Structs.h"
#include "system_header.h"


class CarReader {
public:
    static CarReader &getInstance();
    virtual ~CarReader() {Log2("DTOR CarReader");}//TODO Logger needed (off in release??)

    CarType getCar(const jsoncons::json& data);

private:
    CarReader(): m_car(0) {Log2("CTOR CarReader");}
    void resetData(const jsoncons::json& data);

    const jsoncons::json* m_car; //TODO typedef
};

#endif /* CARREADER_H_ */
