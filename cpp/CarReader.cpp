/*
 * CarReader.cpp
 *
 *  Created on: 20 kwi 2014
 *      Author: Gosia
 */

#include <string>
#include "CarReader.h"

CarReader& CarReader::getInstance()
{
    static CarReader instance;
    return instance;
}

CarType CarReader::getCar(const jsoncons::json& data)
{
    resetData(data);

    std::string name = (*m_car)["name"].as<std::string>(); //TODO not needed2
    std::string color = (*m_car)["color"].as<std::string>();



    CarType car(new Car());
    car->Name = name;
    car->Color = color;

    return car;
}

void CarReader::resetData(const jsoncons::json& data)
{
    m_car = &(data["data"]);
}
