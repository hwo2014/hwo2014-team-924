/*
 * PositionReader.h
 *
 *  Created on: 20 kwi 2014
 *      Author: Gosia
 */

#ifndef POSITIONREADER_H_
#define POSITIONREADER_H_

#include "Log.h"
#include "system_header.h"

class Position;

class PositionReader {
public:
    static PositionReader &getInstance();
    virtual ~PositionReader() {Log2("DTOR Position Reader");}

    //TODO void?
    Position getPosition(const jsoncons::json& data);

private:
    PositionReader(){Log2("CTOR Position Reader");}
    void resetData(const jsoncons::json& data);

    const jsoncons::json* m_position;

};

#endif /* POSITIONREADER_H_ */
