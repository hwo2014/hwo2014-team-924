/*
 * State.cpp
 *
 *  Created on: 20 kwi 2014
 *      Author: Gosia
 */

#include "State.h"

State& State::getInstance()
{
    static State instance;
    return instance;
}
