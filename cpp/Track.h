#ifndef TRACK_H_
#define TRACK_H_

#include <sstream>
#include <memory>
#include "Log.h"
#include "Structs.h"

class Track {
public:
	Track() {Log2("CTOR Track");}
	virtual ~Track() {Log2("DTOR Track");}


	std::string Name;
	std::string Id;

	Pieces pieces;
	Lanes lanes;

//	friend std::ostream& operator
	std::string ToString()
	{
	    std::ostringstream str;
	    str << "Name: "<< Name
	        << "|\t#Pieces: " << pieces.size()
	        << "|\t#Lanes: " << lanes.size();
	    return str.str();
	}
};

typedef std::unique_ptr<Track> TrackPtr; //TODO different name ??

#endif /* TRACK_H_ */
